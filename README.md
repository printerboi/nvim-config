# Nvim-Config

Hey, this is my nvim-config I'm using for my dev-work. Feel free to copy, modify, share and do whatever you like with this config.
I will keep the config updated with plugins I find quite handy.


## Installation

1) Just clone this directoy into you ~/.config/nvim folder and source the init.lua file in the root of the repo
2) Start Nvim with `nvim`
3) Execute `:PackerSync` to install all the plugins
4) Restart Nvim and wait for the the plugins to configure
5) Happy programming :)

## Key-Shortcuts used

- *ctrl+n* : Opens the filetree to the left
- *ctrl+m* : Focus the filetree
- *ctrl+p* : Opens the file-finder
- *Space+Space* : Opens the recently used files
- *Space+f+g* : Opens a window to grep in the files of the directory
